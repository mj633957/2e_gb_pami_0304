import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "M&A Motos";

  cards = [
    {
      titulo: "BMW F 850 GS Premium Plus",
      subtitulo: "R$ 74.500,00",
      conteudo: "Preparada para qualquer tipo de aventura, pode ser adquirida pelo valor acima de R$ 74.000 ou até financiada pela BMW Serviços Financeiros por meio do plano BMW Select. Ao adquirir esse modelo o cliente ganha o emplacamento.",
      foto: "https://motonewsbrasil.com/wp-content/uploads/2018/11/bmw-s-1000-rr-2019-11.jpg",
    },
    {
      titulo: "Honda CB200X",
      subtitulo:"R$ 18.565,00",
      conteudo: "Assim como no mercado brasileiro, os indianos estão começando a pegar gosto pelas aventureiras de baixa cilindrada. A Honda apresentou por lá a CB200X, novidade que chegará em breve às lojas indianas da montadora por 144.000 rúpias, ou cerca de R$ 10.437 na conversão direta da moeda.",
      foto: "https://cdn.motor1.com/images/mgl/xNXnp/s1/honda-cb200x---india.webp"
    },
    {
      titulo: "Yamaha Esportiva R7",
      subtitulo:"R$ 43.800,00",
      conteudo: "A Yamaha preencheu este espaço em sua linha de motos esportivas com a R7, que é baseada na MT-07.",
      foto: "https://cdn.motor1.com/images/mgl/G3yEBJ/s3/yamaha-models-type-approval-docs.jpg"
    },
    {
      titulo: "Suzuki GSX-S750",
      subtitulo:"R$ 24.879,00",
      conteudo: "Interessados em comprar os modelos GSX-S750 e V-Strom 650 XT na versão 2020/2021 terão emplacamento grátis e poderão contar com plano de financiamento com taxa zero ou plano especial com entrada, parcelamento em 36 vezes e parcela final.",
      foto: "https://media.motociclismoonline.com.br/uploads/2021/04/Suzuki-GSX-S750.png"
    }
  ];


  constructor() {}

}
